package com.zzyl.config;

import com.zzyl.properties.SwaggerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Swagger配置类
 */
@Configuration
public class SwaggerConfig {

    @Autowired
    private SwaggerProperties swaggerProperties;

    /**
     * 通过Swagger生成接口文档：管理端接口
     */
    @Bean
    public Docket docket1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("01-智慧养老：管理端接口文档")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zzyl.controller.manager")) //设置扫描包范围
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title(swaggerProperties.getTitle())
                        .version("1.1")
                        .description(swaggerProperties.getDescription())
                        .contact(new Contact(swaggerProperties.getContactName(), swaggerProperties.getContactUrl(), swaggerProperties.getContactEmail()))
                        .build());
    }

    /**
     * 通过Swagger生成接口文档：管理端权限接口
     */
    @Bean
    public Docket docket2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("02-智慧养老：权限功能接口文档")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zzyl.controller.security")) //设置扫描包范围
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title(swaggerProperties.getTitle())
                        .version("1.1")
                        .description(swaggerProperties.getDescription())
                        .contact(new Contact(swaggerProperties.getContactName(), swaggerProperties.getContactUrl(), swaggerProperties.getContactEmail()))
                        .build());
    }

    /**
     * 通过Swagger生成接口文档：微信端接口
     */
    @Bean
    public Docket docket3() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("03-智慧养老：微信端接口文档")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zzyl.controller.customer")) //设置扫描包范围
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title(swaggerProperties.getTitle())
                        .version("1.1")
                        .description(swaggerProperties.getDescription())
                        .contact(new Contact(swaggerProperties.getContactName(), swaggerProperties.getContactUrl(), swaggerProperties.getContactEmail()))
                        .build());
    }
}
