package com.zzyl.config;

import com.zzyl.intercept.AutoFillInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatis配置类
 */
@Configuration
public class MyBatisConfig {

    /***
     *  自动填充拦截器
     */
    @Bean
    public AutoFillInterceptor autoFillInterceptor() {
        return new AutoFillInterceptor();
    }

}
