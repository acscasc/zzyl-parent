package com.zzyl.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvc高级配置类
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

}
