package com.zzyl.enums;

/**
 * 枚举接口
 */
public interface IBasicEnum {

    int getCode();//编码

    String getMsg();//信息
}
