package com.zzyl.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel(description = "Dto基础类")
public class BaseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;//主键
    @JsonIgnore
    private String searchValue;//搜索值
    private String remark;//备注
    @JsonIgnore
    private Map<String, Object> params = new HashMap<>();//请求参数
}


