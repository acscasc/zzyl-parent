package com.zzyl.constant;

public class CacheConstants {

    public static final String RESOURCE = "resource:";
    public static final String LIST = RESOURCE + "list";
    public static final String TREE = RESOURCE + "tree";
    public static final String ACCESS_URLS = "user:access_urls:";
    public static final String IOT_ALL_PRODUCT = "iot:all_product";
    public static final String IOT_DEVICE_LAST_DATA = "iot:device_last_data";

    //报警规则连续触发次数，缓存前缀
    public static final String ALERT_TRIGGER_COUNT = "alert_trigger_count:";
    //报警规则沉默周期，缓存前缀
    public static final String ALERT_SILENT_CYCLE = "alert_silent_cycle:";
}
