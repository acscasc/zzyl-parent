package com.zzyl.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.zzyl.base.BaseEntity;
import lombok.extern.slf4j.Slf4j;

/**
 * management 管理端本地线程维护工具
 */
@Slf4j
public class MgtThreadLocal {

    /**
     * 管理端操作的本地线程
     */
    private static final ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void set(String userVO) {
        threadLocal.set(userVO);
    }

    public static String get() {
        return threadLocal.get();
    }

    public static Long getUserId() {
        String userVO = threadLocal.get();
        if (StrUtil.isNotBlank(userVO)) {
            return JSONUtil.toBean(userVO, BaseEntity.class).getId();
        }
        return null;
    }

    public static void remove() {
        threadLocal.remove();
    }
}
