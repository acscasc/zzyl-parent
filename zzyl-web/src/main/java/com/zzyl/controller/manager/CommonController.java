package com.zzyl.controller.manager;

import cn.hutool.core.util.StrUtil;
import com.zzyl.base.ResponseResult;
import com.zzyl.file.FileStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/common")
@Api(tags = "通用接口")
public class CommonController {

    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/upload")
    @ApiOperation("文件上传")
    public ResponseResult<String> upload(@RequestPart("file") MultipartFile file) throws Exception {
        // 获得原始文件名
        String originalFilename = file.getOriginalFilename();
        // 获得文件扩展名
        String extension = StrUtil.subSuf(originalFilename, originalFilename.lastIndexOf("."));
        String fileName = UUID.randomUUID() + extension;
        String filePath = fileStorageService.store(fileName, file.getInputStream());
        return ResponseResult.success(filePath);
    }

}
