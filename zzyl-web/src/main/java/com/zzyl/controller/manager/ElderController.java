package com.zzyl.controller.manager;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.NursingElderDto;
import com.zzyl.enums.ElderStatusEnum;
import com.zzyl.service.ElderService;
import com.zzyl.vo.ElderCheckInInfoVo;
import com.zzyl.vo.ElderPageQueryVo;
import com.zzyl.vo.retreat.ElderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/elder")
@Api(tags = "客户老人管理")
public class ElderController {

    @Resource
    private ElderService elderService;

    @GetMapping("/selectList")
    @ApiOperation(value = "列表")
    public ResponseResult selectList() {
        List<ElderVo> elderVos = elderService.selectList();
        return ResponseResult.success(elderVos);
    }

    @GetMapping("/pageQuery")
    @ApiOperation(value = "老人分页查询")
    public ResponseResult<PageResponse<ElderPageQueryVo>> pageQuery(@RequestParam(value = "name", required = false) String name,
                                                                    @RequestParam(value = "idCardNo", required = false) String idCardNo,
                                                                    @RequestParam(value = "status", required = false) String status,
                                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        return ResponseResult.success(elderService.pageQuery(name, idCardNo, status, pageNum, pageSize));
    }

    @GetMapping("/checkInInfo/{id}")
    @ApiOperation(value = "老人入住信息查询")
    public ResponseResult<ElderCheckInInfoVo> checkInInfo(@PathVariable("id") Long id) {
        return ResponseResult.success(elderService.checkInInfo(id));
    }

    @PutMapping("/setNursing")
    @ApiOperation(value = "设置护理员", notes = "设置护理员")
    public ResponseResult setNursing(@RequestBody List<NursingElderDto> nursingElders) {
        elderService.setNursing(nursingElders);
        return ResponseResult.success();
    }


    @GetMapping("/selectByIdCard")
    @ApiOperation(value = "身份证号", notes = "身份证号")
    public ResponseResult selectByIdCard(@RequestParam String idCard) {
        ElderVo elderVo = elderService.selectByIdCardAndStatus(idCard, ElderStatusEnum.CHECK_IN.getOrdinal());
        return ResponseResult.success(elderVo);
    }
}


