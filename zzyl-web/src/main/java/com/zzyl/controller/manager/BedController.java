package com.zzyl.controller.manager;

import com.zzyl.base.ResponseResult;
import com.zzyl.dto.BedDto;
import com.zzyl.service.BedService;
import com.zzyl.vo.BedVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/bed")
@Api(tags = "床位管理")
public class BedController {
    @Autowired
    private BedService bedService;


    @PostMapping("/create")
    @ApiOperation("添加床位")
    public ResponseResult<Void> addBed(@RequestBody BedDto bedDto) {
        bedService.addBed(bedDto);
        return ResponseResult.success();
    }

    @GetMapping("/read/{id}")
    @ApiOperation("查询床位信息")
    public ResponseResult<BedVo> getRoom(@PathVariable Long id) {
        BedVo bedVo = bedService.getRoom(id);
        return ResponseResult.success(bedVo);
    }


    @PutMapping(path = "/update")
    @ApiOperation("更新床位")
    public ResponseResult<Void> modifyBed(@RequestBody BedDto bedDto) {
        bedService.modifyBed(bedDto);
        return ResponseResult.success();
    }

    @DeleteMapping(path = "/delete/{id}")
    @ApiOperation("删除床位")
    public ResponseResult<Void> deleteBed(@PathVariable Long id) {
        bedService.deleteBed(id);
        return ResponseResult.success();
    }
}
