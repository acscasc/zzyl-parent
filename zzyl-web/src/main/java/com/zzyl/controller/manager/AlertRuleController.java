package com.zzyl.controller.manager;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/alert-rule")
@Api(tags = "报警规则相关接口")
public class AlertRuleController {

    //TODO @ApiOperation(value = "分页获取告警规则列表")
    //TODO @ApiOperation(value = "新增报警规则")
    //TODO @ApiOperation(value = "获取单个告警规则")
    //TODO @ApiOperation(value = "更新告警规则")
    //TODO @ApiOperation(value = "删除告警规则")
    //TODO @ApiOperation(value = "启用/禁用")
}
