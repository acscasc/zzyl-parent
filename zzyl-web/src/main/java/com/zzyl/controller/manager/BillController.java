package com.zzyl.controller.manager;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.BalanceService;
import com.zzyl.service.BillService;
import com.zzyl.vo.BalanceVo;
import com.zzyl.vo.BillVo;
import com.zzyl.vo.PrepaidRechargeRecordVo;
import com.zzyl.vo.retreat.RetreatBillVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Api(tags = "账单管理")
@RestController
@RequestMapping("/bill")
public class BillController {

    @Autowired
    private BillService billService;
    @Autowired
    private BalanceService balanceService;

    @ApiOperation("根据id查询账单")
    @GetMapping("/{id}")
    public ResponseResult<BillVo> getById(@PathVariable Long id) {
        BillVo billVo = billService.selectByPrimaryKey(id);
        return ResponseResult.success(billVo);
    }

    @ApiOperation("分页查询账单")
    @GetMapping("/page/")
    public ResponseResult<PageResponse<BillVo>> getBillPage(@RequestParam(name = "billNo", required = false) String billNo,
                                                            @RequestParam(name = "elderName", required = false) String elderName,
                                                            @RequestParam(name = "elderIdCard", required = false) String elderIdCard,
                                                            @RequestParam(name = "startTime", required = false) Long startTime,
                                                            @RequestParam(name = "endTime", required = false) Long endTime,
                                                            @RequestParam(name = "transactionStatus", required = false) Integer transactionStatus,
                                                            @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageResponse<BillVo> billPage = billService.getBillPage(billNo, elderName, elderIdCard,
                ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime),
                ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime),
                transactionStatus, null, pageNum, pageSize);
        return ResponseResult.success(billPage);
    }

    @ApiOperation("分页查询欠费账单")
    @GetMapping("/arrears/")
    public ResponseResult<PageResponse<BillVo>> arrears(@RequestParam(name = "bedNo", required = false) String bedNo,
                                                        @RequestParam(name = "elderName", required = false) String elderName,
                                                        @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                        @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageResponse<BillVo> arrears = billService.arrears(bedNo, elderName, pageNum, pageSize);
        return ResponseResult.success(arrears);
    }

    @ApiOperation("分页查询余额")
    @GetMapping("/balance/")
    public ResponseResult<PageResponse<BalanceVo>> balance(@RequestParam(name = "bedNo", required = false) String bedNo,
                                                           @RequestParam(name = "elderName", required = false) String elderName,
                                                           @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                           @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageResponse<BalanceVo> balanceVoPageResponse = balanceService.page(bedNo, elderName, pageNum, pageSize);
        return ResponseResult.success(balanceVoPageResponse);
    }


    @ApiOperation("分页查询预付费充值记录")
    @GetMapping("/prepaidRechargeRecord/page")
    public ResponseResult<PageResponse<PrepaidRechargeRecordVo>> prepaidRechargeRecordPage(@RequestParam(name = "bedNo", required = false) String bedNo,
                                                                                           @RequestParam(name = "elderName", required = false) String elderName,
                                                                                           @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                                                           @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageResponse<PrepaidRechargeRecordVo> prepaidRechargeRecordVoPageResponse = billService.prepaidRechargeRecordPage(bedNo, elderName, pageNum, pageSize);
        return ResponseResult.success(prepaidRechargeRecordVoPageResponse);
    }

    @ApiOperation("退住费用清算查询")
    @GetMapping("/retreatSettlement")
    public ResponseResult<RetreatBillVo> retreatSettlement(@RequestParam("elderId") Long elderId,
                                                           @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam("checkOutTime") LocalDateTime checkOutTime) {
        return ResponseResult.success(billService.retreatSettlement(elderId, checkOutTime));
    }
}
