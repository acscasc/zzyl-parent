package com.zzyl.controller.security;

import com.zzyl.base.ResponseResult;
import com.zzyl.service.ResourceService;
import com.zzyl.vo.MenuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/resource")
@Api(tags = "资源管理")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @GetMapping("/menus")
    @ApiOperation("我的菜单")
    public ResponseResult<List<MenuVo>> menus() {
        Long userId = 1671403256519078138L;
        List<MenuVo> menus = resourceService.menus(userId);
        return ResponseResult.success(menus);
    }
}
