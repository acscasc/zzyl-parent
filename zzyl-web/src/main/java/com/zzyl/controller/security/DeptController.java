package com.zzyl.controller.security;


import com.zzyl.base.ResponseResult;
import com.zzyl.dto.DeptDto;
import com.zzyl.service.DeptService;
import com.zzyl.vo.DeptVo;
import com.zzyl.vo.TreeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "部门管理")
@RestController
@RequestMapping("/dept")
public class DeptController {

    @Autowired
    private DeptService deptService;

    @PutMapping
    @ApiOperation(value = "部门添加", notes = "部门添加")
    public ResponseResult<Void> createDept(@RequestBody DeptDto deptDto) {
        deptService.createDept(deptDto);
        return ResponseResult.success();
    }

    @PatchMapping
    @ApiOperation(value = "部门修改", notes = "部门修改")
    public ResponseResult<Void> updateDept(@RequestBody DeptDto deptDto) {
        deptService.updateDept(deptDto);
        return ResponseResult.success();
    }

    @PatchMapping("/is_enable")
    @ApiOperation(value = "启用-禁用", notes = "启用-禁用")
    public ResponseResult<Void> isEnable(@RequestBody DeptDto deptDto) {
        deptService.isEnable(deptDto);
        return ResponseResult.success();
    }

    @ApiOperation("删除部门")
    @DeleteMapping("/{deptNo}")
    public ResponseResult<Void> remove(@PathVariable String deptNo) {
        deptService.deleteByDeptNo(deptNo);
        return ResponseResult.success();
    }

    @PostMapping("/list")
    @ApiOperation(value = "部门列表", notes = "部门列表")
    public ResponseResult<List<DeptVo>> deptList(@RequestBody DeptDto deptDto) {
        List<DeptVo> deptVoList = deptService.findDeptList(deptDto);
        return ResponseResult.success(deptVoList);
    }

    @PostMapping("/tree")
    @ApiOperation(value = "部门树形", notes = "部门树形")
    public ResponseResult<TreeVo> deptTreeVo(@RequestBody DeptDto deptDto) {
        TreeVo treeVo = deptService.deptTreeVo(deptDto);
        return ResponseResult.success(treeVo);
    }
}
