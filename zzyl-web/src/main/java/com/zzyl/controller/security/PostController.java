package com.zzyl.controller.security;


import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.dto.PostDto;
import com.zzyl.service.PostService;
import com.zzyl.vo.PostVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "岗位管理")
@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation(value = "岗位分页", notes = "岗位分页")
    public ResponseResult<PageResponse<PostVo>> findPostVoPageResponse(@RequestBody PostDto postDto,
                                                                       @PathVariable("pageNum") int pageNum,
                                                                       @PathVariable("pageSize") int pageSize) {
        PageResponse<PostVo> pageResponse = postService.findPostPage(postDto, pageNum, pageSize);
        return ResponseResult.success(pageResponse);
    }

    @PutMapping
    @ApiOperation(value = "岗位添加", notes = "岗位添加")
    public ResponseResult<Void> createPost(@RequestBody PostDto postDto) {
        postService.createPost(postDto);
        return ResponseResult.success();
    }

    @PatchMapping
    @ApiOperation(value = "岗位修改", notes = "岗位修改")
    public ResponseResult<Void> updatePost(@RequestBody PostDto postDto) {
        postService.updatePost(postDto);
        return ResponseResult.success();
    }

    @PatchMapping("/is_enable")
    @ApiOperation(value = "启用-禁用", notes = "启用-禁用")
    public ResponseResult<Void> isEnable(@RequestBody PostDto postDto) {
        postService.isEnable(postDto);
        return ResponseResult.success();
    }

    @ApiOperation("删除岗位")
    @DeleteMapping("/{postId}")
    public ResponseResult<Void> remove(@PathVariable String postId) {
        postService.deletePostById(postId);
        return ResponseResult.success();
    }

    @PostMapping("/list")
    @ApiOperation(value = "岗位列表", notes = "岗位列表")
    public ResponseResult<List<PostVo>> postList(@RequestBody PostDto postDto) {
        List<PostVo> postVoList = postService.findPostList(postDto);
        return ResponseResult.success(postVoList);
    }
}
