package com.zzyl.controller.customer;

import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.BillService;
import com.zzyl.vo.BillVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "客户账单管理")
@RestController
@RequestMapping("/customer/bill")
public class CustomerBillController {

    @Autowired
    private BillService billService;

    @ApiOperation("分页查询账单")
    @GetMapping("/page/")
    public ResponseResult<PageResponse<BillVo>> getBillPage(@RequestParam(name = "transactionStatus", required = false) Integer transactionStatus,
                                                            @RequestParam(name = "elderId", required = false) Long elderId,
                                                            @RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
                                                            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PageResponse<BillVo> billPage = billService.getBillPage(null, null, null, null, null,
                transactionStatus, elderId, pageNum, pageSize);
        return ResponseResult.success(billPage);
    }

    @ApiOperation("根据id查询账单")
    @GetMapping("/{id}")
    public ResponseResult<BillVo> getById(@PathVariable Long id) {
        BillVo billVo = billService.selectByPrimaryKey(id);
        return ResponseResult.success(billVo);
    }


}
