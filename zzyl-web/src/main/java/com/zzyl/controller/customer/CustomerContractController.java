package com.zzyl.controller.customer;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.ContractService;
import com.zzyl.vo.ContractVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer/contract")
@Api(tags = "客户合同管理")
public class CustomerContractController {

    @Autowired
    private ContractService contractService;

    @ApiOperation(value = "分页查询合同信息")
    @GetMapping("/list")
    public ResponseResult<PageResponse<ContractVo>> selectByPage(Integer pageNum,
                                                                 Integer pageSize,
                                                                 String contractNo,
                                                                 String elderName,
                                                                 Integer status,
                                                                 Long startTime,
                                                                 Long endTime) {
        PageResponse<ContractVo> pageInfo = contractService.selectByPage(
                pageNum, pageSize,
                contractNo, elderName, status,
                ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime),
                ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime)
        );
        return ResponseResult.success(pageInfo);
    }
}

