package com.zzyl.controller.customer;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.zzyl.base.PageResponse;
import com.zzyl.base.ResponseResult;
import com.zzyl.service.OrderService;
import com.zzyl.vo.OrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "客户订单管理")
@RestController
@RequestMapping("/customer/orders")
@Slf4j
public class CustomOrderController {

    //TODO @ApiOperation("分页查询护理项目列表")
    //TODO @ApiOperation("根据编号查询护理项目信息")

    ///////////////////////////////////////////////////////////////////////////////

    @Autowired
    private OrderService orderService;

    @ApiOperation("分页查询订单")
    @GetMapping("/order/page")
    public ResponseResult<PageResponse<OrderVo>> searchOrders(@RequestParam(value = "status", required = false) Integer status,
                                                              @RequestParam(value = "orderNo", required = false) String orderNo,
                                                              @RequestParam(value = "elderlyName", required = false) String elderlyName,
                                                              @RequestParam(value = "creator", required = false) String creator,
                                                              @RequestParam(value = "startTime", required = false) Long startTime,
                                                              @RequestParam(value = "endTime", required = false) Long endTime,
                                                              @RequestParam("pageNum") Integer pageNum,
                                                              @RequestParam("pageSize") Integer pageSize) {
        PageResponse<OrderVo> listPageResponse = orderService.searchOrders(status, orderNo, elderlyName, creator,
                ObjectUtil.isEmpty(startTime) ? null : LocalDateTimeUtil.of(startTime),
                ObjectUtil.isEmpty(endTime) ? null : LocalDateTimeUtil.of(endTime),
                pageNum, pageSize);
        return ResponseResult.success(listPageResponse);
    }
}

