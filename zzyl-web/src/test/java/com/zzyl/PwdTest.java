package com.zzyl;

import cn.hutool.crypto.digest.BCrypt;
import cn.hutool.crypto.digest.DigestUtil;

public class PwdTest {

    public static void main(String[] args) {
        //md5加密
        String md5Pswd1 = DigestUtil.md5Hex("123456");
        String md5Pswd2 = DigestUtil.md5Hex("123456");
        System.out.println(md5Pswd1);
        System.out.println(md5Pswd2);

        //BCrypt加密
        String hashpw1 = BCrypt.hashpw("123456", BCrypt.gensalt());
        String hashpw2 = BCrypt.hashpw("123456", BCrypt.gensalt());
        System.out.println(hashpw1);
        System.out.println(hashpw2);

        //验证密码是否正确
        boolean checkpw = BCrypt.checkpw("123456", "$2a$10$QPYqZUGv6w53RWOH3hU.gOc4vIOU4Uysif1e.7sQRDIU3BeQsEcZS");
        System.out.println(checkpw);

    }
}
