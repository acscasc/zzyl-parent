package com.zzyl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadLocalTest {

    private static final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();//内部结构，就是一个map

    public static void main(String[] args) {

        Thread t1 = new Thread(() -> {
            THREAD_LOCAL.set("itheima");

            log.info(THREAD_LOCAL.get());
        }, "t1");

        Thread t2 = new Thread(() -> {
            THREAD_LOCAL.set("itcast");

            log.info(THREAD_LOCAL.get());
        }, "t2");

        t1.start();
        t2.start();
    }
}