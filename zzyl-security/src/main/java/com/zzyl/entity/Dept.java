package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Dept extends BaseEntity {

    private String parentDeptNo;//父部门编号
    private String deptNo;//部门编号
    private String deptName;//部门名称
    private Integer sortNo;//排序
    private String dataState;//数据状态（0正常 1停用）
    private Long leaderId;//负责人Id
    private String leaderName;//负责人姓名
    private Integer childCount;//子部门的数量
}
