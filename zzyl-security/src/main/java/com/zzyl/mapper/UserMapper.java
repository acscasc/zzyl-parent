package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.dto.UserDto;
import com.zzyl.entity.User;
import com.zzyl.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int batchInsert(@Param("list") List<User> list);

    Page<List<User>> selectPage(UserDto userDto);

    Page<List<UserVo>> getPage(UserDto userDto);

    @Select("select count(id) from sys_user where post_no = #{postNo}")
    Integer checkPostHasUser(String postNo);

    @Select("SELECT COUNT(1) FROM sys_user where dept_no = #{postNo}")
    int checkDeptExistUser(String postNo);

    @Select("select * from sys_user")
    List<User> selectList();

    @Select("select * from sys_user where username = #{username}")
    User selectByUsername(String username);

    List<User> selectUserByIds(@Param("userIds") List<Long> userIds);
}