package com.zzyl.mapper;

import com.zzyl.vo.MenuVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ResourceMapper {

    List<MenuVo> selectMenuByUserId(Long userId);
}