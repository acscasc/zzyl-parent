package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel("部门VO")
public class DeptVo extends BaseVo {

    private String parentDeptNo; // 父部门编号
    private String deptNo; // 部门编号
    private String deptName; // 部门名称
    private Integer sortNo; // 排序
    private Long leaderId; // 负责人Id
    private String leaderName; // 负责人姓名
    private String roleId; // 角色查询部门：部门对应角色id
    private Integer level = 4; // 层级
    private Integer childCount; // 子部门的数量
}
