package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("菜单说明VO")
public class MenuMetaVo implements Serializable {

    private String title; // 标题
    private String icon; // 图标
    private List<String> roles; // 角色
}
