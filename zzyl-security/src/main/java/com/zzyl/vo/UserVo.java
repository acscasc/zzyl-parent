package com.zzyl.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zzyl.base.BaseVo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * 用户表
 */
@Data
@NoArgsConstructor
public class UserVo extends BaseVo {

    private String username; // 用户账号
    @JsonIgnore
    private String password; // 密码
    private String userType; // 用户类型（0:系统用户,1:客户）
    private String nickName; // 用户昵称
    private String post; // 用户职位
    private String dept; // 用户部门
    private String email; // 用户邮箱
    private String realName; // 真实姓名
    private String mobile; // 手机号码
    private String sex; // 用户性别（0男 1女 2未知）
    private String remark; // 备注
    private String[] checkedIds; // 选中节点
    private String openId; // 三方openId

    private Set<String> roleVoIds; // 查询用户：用户角色Ids
    private Set<String> roleLabels; // 构建令牌：用户角色标识
    private Set<String> resourceRequestPaths; // 构建令牌：用户权限路径

    private String deptNo; // 部门编号【当前】
    private String postNo; // 职位编号【当前】
    private Long roleId; // 角色Id【当前】
    private String userToken; // 用户令牌
    private String dataState;
    private String deptName; // 部门名称
    private String postName; // 岗位名称
}
