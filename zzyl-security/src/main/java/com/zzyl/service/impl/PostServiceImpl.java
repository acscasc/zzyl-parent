package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.PostDto;
import com.zzyl.entity.Post;
import com.zzyl.enums.BasicEnum;
import com.zzyl.exception.BaseException;
import com.zzyl.mapper.DeptMapper;
import com.zzyl.mapper.PostMapper;
import com.zzyl.mapper.UserMapper;
import com.zzyl.service.DeptService;
import com.zzyl.service.PostService;
import com.zzyl.utils.NoProcessing;
import com.zzyl.vo.DeptVo;
import com.zzyl.vo.PostVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 岗位表服务实现类
 */
@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostMapper postMapper;
    @Autowired
    DeptService deptService;
    @Autowired
    DeptMapper deptMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     * 多条件查询岗位表分页列表
     *
     * @param postDto  查询条件
     * @param pageNum  页码
     * @param pageSize 每页条数
     * @return Page<PostVo>
     */
    @Override
    public PageResponse<PostVo> findPostPage(PostDto postDto, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        if (ObjectUtil.isEmpty(postDto.getDeptNo())) {
            throw new BaseException(BasicEnum.DEPT_NULL_EXCEPTION);
        }

        //处理部门编号
        postDto.setDeptNo(NoProcessing.processString(postDto.getDeptNo()));

        Page<Post> page = postMapper.selectPage(postDto);
        PageResponse<PostVo> pageResponse = PageResponse.of(page, PostVo.class);
        if (ObjectUtil.isNotEmpty(pageResponse.getRecords())) {
            //对应部门
            List<String> deptNos = pageResponse.getRecords().stream().map(PostVo::getDeptNo).collect(Collectors.toList());
            List<DeptVo> deptVoList = deptService.findDeptInDeptNos(deptNos);
            pageResponse.getRecords().forEach(n -> {
                n.setCreateDay(LocalDateTimeUtil.format(n.getCreateTime(), "yyyy-MM-dd"));
                //装配部门
                deptVoList.forEach(d -> {
                    if (n.getDeptNo().equals(d.getDeptNo())) {
                        n.setDeptVo(BeanUtil.toBean(d, DeptVo.class));
                    }
                });
            });
        }
        return pageResponse;
    }


    /**
     * 创建岗位表
     *
     * @param postDto 对象信息
     * @return PostDto
     */
    @Override
    public void createPost(PostDto postDto) {
        //转换PostVo为Post
        //判断是否是最底层部门，如果不是，则提示：请选择最底层部门
        String deptNo = postDto.getDeptNo();
        if (deptService.isLowestDept(deptNo)) {
            throw new RuntimeException("请选择最底层部门");
        }

        Post post = BeanUtil.toBean(postDto, Post.class);
        String postNo = createPostNo(post.getDeptNo());
        post.setPostNo(postNo);
        postMapper.insert(post);
    }

    /**
     * 修改岗位表
     *
     * @param postDto 对象信息
     * @return Boolean
     */
    @Override
    public void updatePost(PostDto postDto) {

        //判断是否是最底层部门，如果不是，则提示：请选择最底层部门
        String deptNo = postDto.getDeptNo();
        if (deptService.isLowestDept(deptNo)) {
            throw new RuntimeException("请选择最底层部门");
        }

        //转换PostVo为Post
        Post post = BeanUtil.toBean(postDto, Post.class);
        postMapper.updateByPrimaryKey(post);
    }


    /**
     * 职位启用和禁用
     *
     * @param postDto
     * @return
     */
    @Override
    public void isEnable(PostDto postDto) {

        //TODO 如果是禁用，则需要判断是否已经被用户引用 已完成
        if (postDto.getDataState().equals("1")) {
            Post p = postMapper.selectById(postDto.getId());
            Integer count = checkPostHasUser(p.getPostNo());
            if (count > 0) {
                throw new BaseException(BasicEnum.POSITION_DISTRIBUTED);
            }
        }


        Post post = BeanUtil.toBean(postDto, Post.class);
        postMapper.updateByPrimaryKeySelective(post);
    }

    /**
     * 校验岗位是否分配用户
     *
     * @return
     */
    private Integer checkPostHasUser(String postNo) {
        return userMapper.checkPostHasUser(postNo);
    }

    @Override
    public List<PostVo> findPostList(PostDto postDto) {

        postDto.setDataState("0");
        List<Post> postList = postMapper.selectList(postDto);
        List<PostVo> postVoList = BeanUtil.copyToList(postList, PostVo.class);
        return postVoList;
    }

    @Override
    public List<PostVo> findPostVoListByUserId(Long userId) {
        return postMapper.findPostVoListByUserId(userId);
    }

    /**
     * 创建编号
     *
     * @param deptNo
     * @return
     */
    private String createPostNo(String deptNo) {
        PostDto postDto = PostDto.builder().deptNo(deptNo).build();
        List<Post> postList = postMapper.selectList(postDto);
        //无下属节点则创建下属节点
        if (ObjectUtil.isEmpty(postList)) {
            return NoProcessing.createNo(deptNo, false);
            //有下属节点则累加下属节点
        } else {
            Long postNo = postList.stream()
                    .map(post -> Long.valueOf(post.getPostNo()))
                    .max(Comparator.comparing(i -> i)).get();
            return NoProcessing.createNo(String.valueOf(postNo), true);
        }
    }

    @Override
    public void deletePostById(String postNo) {
        //TODO 验证当前岗位是否被用户关联  已完成
        Integer total = checkPostHasUser(postNo);
        if (total > 0) {
            throw new RuntimeException("职位已分配，不允许删除");
        }
        postMapper.deleteByPostNo(postNo);
    }
}
