package com.zzyl.runner;

import lombok.Data;

import java.util.Map;

/**
 * Iot设备上报后，接收的数据格式
 */
@Data
public class IotContent {

    private String deviceType;//设备类型
    private String iotId;//设备ID
    private long requestId;//请求ID
    private long gmtCreate;
    private String productKey;//产品key
    private String deviceName;//设备名称
    private Map<String, Object> checkFailedData;
    private Map<String, Item> items;//数据项

    @Data
    public static class Item {
        private int value;
        private long time;
    }
}