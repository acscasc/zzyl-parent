package com.zzyl.service;

import com.zzyl.base.PageResponse;
import com.zzyl.dto.AlertDataHandleDto;
import com.zzyl.dto.AlertDataPageQueryDto;
import com.zzyl.vo.AlertDataVo;

public interface AlertDataService {
    /**
     * 管理端分页查询报警数据
     *
     * @param alertDataPageQueryDto 报警数据分页查询请求
     * @return 报警数据
     */
    PageResponse<AlertDataVo> adminPageQuery(AlertDataPageQueryDto alertDataPageQueryDto);

    /**
     * 处理设备报警数据
     *
     * @param id                 报警数据id
     * @param alertDataHandleDto 处理报警数据请求模型
     */
    void handleAlertData(Long id, AlertDataHandleDto alertDataHandleDto);

}
