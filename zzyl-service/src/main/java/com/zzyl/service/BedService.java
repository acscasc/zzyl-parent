package com.zzyl.service;

import com.zzyl.dto.BedDto;
import com.zzyl.vo.BedVo;

public interface BedService {
    void addBed(BedDto bedDto);

    BedVo getRoom(Long id);

    void modifyBed(BedDto bedDto);

    void deleteBed(Long id);
}
