package com.zzyl.service;

import com.zzyl.base.PageResponse;
import com.zzyl.dto.CheckInApplyDto;
import com.zzyl.vo.CheckInDetailVo;
import com.zzyl.vo.CheckInPageQueryVo;

public interface CheckInService {
    /**
     * 申请入住
     *
     * @param checkInApplyDto 申请入住请求模型
     */
    void apply(CheckInApplyDto checkInApplyDto);

    /**
     * 分页查询
     *
     * @param elderName 老人姓名，模糊查询
     * @param idCardNo  身份证号，精确查询
     * @param pageNum   页码
     * @param pageSize  页面大小
     * @return 分页结果
     */
    PageResponse<CheckInPageQueryVo> pageQuery(String elderName, String idCardNo, Integer pageNum, Integer pageSize);

    /**
     * 入住详情
     *
     * @param id 入住id
     * @return 入住详情
     */
    CheckInDetailVo detail(Long id);
}
