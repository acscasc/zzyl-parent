package com.zzyl.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.AlertDataHandleDto;
import com.zzyl.dto.AlertDataPageQueryDto;
import com.zzyl.entity.AlertData;
import com.zzyl.entity.User;
import com.zzyl.mapper.AlertDataMapper;
import com.zzyl.service.AlertDataService;
import com.zzyl.utils.MgtThreadLocal;
import com.zzyl.vo.AlertDataVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
public class AlertDataServiceImpl implements AlertDataService {


    @Autowired
    private AlertDataMapper alertDataMapper;

    /**
     * 管理端分页查询报警数据
     *
     * @param alertDataPageQueryDto 报警数据分页查询请求
     * @return 报警数据
     */
    @Override
    public PageResponse<AlertDataVo> adminPageQuery(AlertDataPageQueryDto alertDataPageQueryDto) {
        // 获取当前的用户
        Long userId = MgtThreadLocal.getUserId();

        PageHelper.startPage(alertDataPageQueryDto.getPageNum(), alertDataPageQueryDto.getPageSize());
        LocalDateTime startTime = alertDataPageQueryDto.getStartTime() == null ? null : LocalDateTimeUtil.of(alertDataPageQueryDto.getStartTime());
        LocalDateTime endTime = alertDataPageQueryDto.getEndTime() == null ? null : LocalDateTimeUtil.of(alertDataPageQueryDto.getEndTime());

        Page<AlertDataVo> pageResult = alertDataMapper.adminPageQuery(alertDataPageQueryDto.getId(), alertDataPageQueryDto.getStatus(), alertDataPageQueryDto.getDeviceName(), userId, startTime, endTime);
        return PageResponse.of(pageResult, AlertDataVo.class);
    }

    /**
     * 处理设备报警数据
     *
     * @param id                 报警数据id
     * @param alertDataHandleDto 处理报警数据请求模型
     */
    @Transactional
    @Override
    public void handleAlertData(Long id, AlertDataHandleDto alertDataHandleDto) {

        AlertData alertData = alertDataMapper.selectByPrimaryKey(id);

        //从当前线程中获取用户数据，更新报警数据
        String subject = MgtThreadLocal.get();
        User user = JSON.parseObject(subject, User.class);
        alertDataMapper.handleAlertData(id, alertDataHandleDto.getProcessingResult(), alertDataHandleDto.getProcessingTime(), user.getRealName(), user.getId(), alertData.getIotId());

        String iotId = alertData.getIotId();
        Long userId = user.getId();
        String realName = user.getRealName();

        alertDataMapper.updateStatus(iotId, userId, realName, alertDataHandleDto.getProcessingResult(), alertDataHandleDto.getProcessingTime());
    }

}
