package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.zzyl.dto.BedDto;
import com.zzyl.entity.Bed;
import com.zzyl.enums.BasicEnum;
import com.zzyl.exception.BaseException;
import com.zzyl.mapper.BedMapper;
import com.zzyl.service.BedService;
import com.zzyl.vo.BedVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class BedServiceImpl implements BedService {
    @Autowired
    private BedMapper bedMapper;

    /**
     * 新增床位
     *
     * @param bedDto
     */
    @Override
    public void addBed(BedDto bedDto) {
        Bed bed = new Bed();
        BeanUtil.copyProperties(bedDto, bed);
        bed.setCreateBy(1L);
        bed.setUpdateBy(1L);
        bed.setUpdateTime(LocalDateTime.now());
        bed.setCreateTime(LocalDateTime.now());
        bed.setBedStatus(0);
        bedMapper.insert(bed);
    }

    @Override
    public BedVo getRoom(Long id) {
        Bed bed = bedMapper.getBedById(id);
        BedVo bedVo = new BedVo();
        BeanUtil.copyProperties(bed, bedVo);
        return bedVo;
    }

    @Override
    public void modifyBed(BedDto bedDto) {
        Long id = bedDto.getId();
        Bed bed = bedMapper.getBedById(id);
        if (bed == null) {
            throw new BaseException(BasicEnum.DATA_NOT_EXITS);
        }
        bed.setUpdateTime(LocalDateTime.now());
        bed.setBedNumber(bedDto.getBedNumber());
        bed.setSort(bedDto.getSort());
        bed.setRoomId(bedDto.getRoomId());
        bed.setUpdateBy(1L);
        bedMapper.modifyBed(bed);
    }

    @Override
    public void deleteBed(Long id) {
        bedMapper.deleteBed(id);
    }
}
