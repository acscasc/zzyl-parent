package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.zzyl.dto.RoomTypeDto;
import com.zzyl.entity.RoomType;
import com.zzyl.file.FileStorageService;
import com.zzyl.mapper.RoomTypeMapper;
import com.zzyl.service.RoomTypeService;
import com.zzyl.vo.RoomTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomTypeServiceImpl implements RoomTypeService {

    @Autowired
    private RoomTypeMapper roomTypeMapper;
    @Autowired
    private FileStorageService fileStorageService;

    /**
     * 添加房间类型
     *
     * @param roomTypeDTO 房间类型DTO
     */
    @Override
    public void addRoomType(RoomTypeDto roomTypeDTO) {
        RoomType roomType = BeanUtil.toBean(roomTypeDTO, RoomType.class);
        roomTypeMapper.addRoomType(roomType);
    }

    /**
     * 删除房间类型
     *
     * @param id 房间类型id
     */
    @Override
    public void removeRoomType(Long id) {
        //先删除图片
        RoomType roomType = roomTypeMapper.findRoomTypeById(id);
        fileStorageService.delete(roomType.getPhoto());
        //删除房间类型
        roomTypeMapper.removeRoomType(id);
    }

    /**
     * 修改房间类型
     *
     * @param id          房间类型id
     * @param roomTypeDTO 房间类型DTO
     */
    @Override
    public void modifyRoomType(Long id, RoomTypeDto roomTypeDTO) {
        RoomType roomType = BeanUtil.toBean(roomTypeDTO, RoomType.class);
        roomType.setId(id);
        roomTypeMapper.modifyRoomType(roomType);
    }

    /**
     * 根据id查找房间类型
     *
     * @param id 房间类型id
     * @return 房间类型VO
     */
    @Override
    public RoomTypeVo findRoomTypeById(Long id) {
        RoomType roomType = roomTypeMapper.findRoomTypeById(id);
        return BeanUtil.toBean(roomType, RoomTypeVo.class);
    }

    /**
     * 查找所有房间类型
     *
     * @return 房间类型VO列表
     */
    @Override
    public List<RoomTypeVo> findRoomTypeList() {
        List<RoomType> roomTypes = roomTypeMapper.findRoomTypeList();
        return BeanUtil.copyToList(roomTypes, RoomTypeVo.class);
    }

    /**
     * 根据状态查找房间类型
     *
     * @param status 状态
     * @return 房间类型VO列表
     */
    @Override
    public List<RoomTypeVo> findRoomTypeListByStatus(Integer status) {
        List<RoomType> roomTypes = roomTypeMapper.findRoomTypeListByStatus(status);
        return BeanUtil.copyToList(roomTypes, RoomTypeVo.class);
    }

    /**
     * 根据类型名查找房间类型
     *
     * @param typeName 类型名
     * @return 房间类型VO列表
     */
    @Override
    public List<RoomTypeVo> findRoomTypeListByTypeName(String typeName) {
        List<RoomType> roomTypes = roomTypeMapper.findRoomTypeListByTypeName(typeName);
        return BeanUtil.copyToList(roomTypes, RoomTypeVo.class);
    }

    @Override
    public void enableOrDisable(Long id, Integer status) {
        roomTypeMapper.updateStatus(id, status);
    }
}
