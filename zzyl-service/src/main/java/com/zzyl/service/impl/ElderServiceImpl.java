package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zzyl.base.PageResponse;
import com.zzyl.dto.ElderDto;
import com.zzyl.dto.NursingElderDto;
import com.zzyl.entity.Elder;
import com.zzyl.entity.NursingElder;
import com.zzyl.enums.ElderStatusEnum;
import com.zzyl.mapper.ElderMapper;
import com.zzyl.mapper.NursingElderMapper;
import com.zzyl.service.ElderService;
import com.zzyl.vo.ElderCheckInInfoVo;
import com.zzyl.vo.ElderPageQueryVo;
import com.zzyl.vo.UserVo;
import com.zzyl.vo.retreat.ElderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ElderServiceImpl implements ElderService {

    @Autowired
    private ElderMapper elderMapper;

    @Resource
    private NursingElderMapper nursingElderMapper;

    /**
     * 根据id删除老人信息
     */
    @Override
    public int deleteByPrimaryKey(Long id) {
        return elderMapper.deleteByPrimaryKey(id);
    }

    /**
     * 新增或更新老人信息
     *
     * @param elder 新增或更新老人模型
     * @return 老人信息
     */
    @Override
    public Elder insertOrUpdate(Elder elder) {
        //1.根据身份证号和退住状态查询老人
        ElderVo elderVo = selectByIdCardAndStatus(elder.getIdCardNo(), ElderStatusEnum.RETREAT.getOrdinal());

        //2.如果存在退住状态的老人，就更新，否则就是新增
        if (ObjectUtil.isNotEmpty(elderVo)) {
            elder.setId(elderVo.getId());
            elderMapper.updateByPrimaryKeySelective(elder);
        } else {
            elderMapper.insert(elder);
        }
        return elder;
    }

    /**
     * 选择性插入老人信息
     */
    @Override
    public int insertSelective(ElderDto elderDto) {
        Elder elder = BeanUtil.toBean(elderDto, Elder.class);
        return elderMapper.insertSelective(elder);
    }

    /**
     * 根据id选择老人信息
     */
    @Override
    public ElderVo selectByPrimaryKey(Long id) {
        Elder elder = elderMapper.selectByPrimaryKey(id);
        return BeanUtil.toBean(elder, ElderVo.class);
    }

    /**
     * 选择性更新老人信息
     */
    @Override
    public Elder updateByPrimaryKeySelective(ElderDto elderDto, boolean b) {
        Elder elder = BeanUtil.toBean(elderDto, Elder.class);
        if (b) {
            elder.setRemark("0");
            ElderVo elderVo = selectByIdCardAndName(elderDto.getIdCardNo(), elderDto.getName());
            if (ObjectUtil.isNotEmpty(elderVo)) {
                int i = Integer.parseInt(elderVo.getRemark()) + 1;
                elder.setName(elder.getName() + i);
                elderVo.setRemark(i + "");
                Elder elder1 = BeanUtil.toBean(elderVo, Elder.class);
                elderMapper.updateByPrimaryKeySelective(elder1);
                return elder1;
            }
        }
        elderMapper.updateByPrimaryKeySelective(elder);
        return elder;
    }

    /**
     * 更新老人信息
     */
    @Override
    public int updateByPrimaryKey(ElderDto elderDto) {
        Elder elder = BeanUtil.toBean(elderDto, Elder.class);
        return elderMapper.updateByPrimaryKey(elder);
    }


    /**
     * 根据身份证号和姓名查询老人信息
     */
    @Override
    public ElderVo selectByIdCardAndName(String idCard, String name) {
        Elder elder = elderMapper.selectByIdCardAndName(idCard, name);
        return BeanUtil.toBean(elder, ElderVo.class);
    }


    /**
     * 根据身份证号和姓名查询老人信息
     */
    @Override
    public List<ElderVo> selectList() {
        List<Elder> elder = elderMapper.selectList();
        return BeanUtil.copyToList(elder, ElderVo.class);
    }

    @Override
    public List<Elder> selectByIds(List<Long> ids) {
        return elderMapper.selectByIds(ids);
    }

    @Override
    public void setNursing(List<NursingElderDto> nursingElders) {
        List<NursingElder> nursingElderList = new ArrayList<>();
        nursingElders.forEach(v -> {
            v.getNursingIds().forEach(v1 -> {
                NursingElder nursingElder = BeanUtil.toBean(v, NursingElder.class);
                nursingElder.setNursingId(v1);
                nursingElderList.add(nursingElder);
            });
        });
        nursingElderMapper.deleteByElderIds(nursingElders.stream().map(NursingElderDto::getElderId).collect(Collectors.toList()).toArray(Long[]::new));
        nursingElderMapper.batchInsert(nursingElderList);
    }


    /**
     * 根据身份证号和姓名查询老人信息
     */
    @Override
    public ElderVo selectByIdCardAndStatus(String idCard, String name) {
        Elder elder = elderMapper.selectByIdCardAndName(idCard, name);
        return BeanUtil.toBean(elder, ElderVo.class);
    }

    /**
     * 根据身份证号和状态查询老人
     *
     * @param idCard 身份证号
     * @param status 状态{@link ElderStatusEnum}
     * @return 老人信息
     */
    @Override
    public ElderVo selectByIdCardAndStatus(String idCard, Integer status) {
        return BeanUtil.toBean(elderMapper.selectByIdCardAndStatus(idCard, status), ElderVo.class);
    }

    /**
     * 清除老人床位编号
     *
     * @param elderId
     */
    @Override
    public void clearBedNum(Long elderId) {
        elderMapper.clearBedNum(elderId);
    }

    /**
     * 老人信息分页查询
     *
     * @param name     老人姓名，模糊查询
     * @param idCardNo 身份证号，精确查询
     * @param status   状态，0：入住中，1：已退住
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @return 分页结果
     */
    @Override
    public PageResponse<ElderPageQueryVo> pageQuery(String name, String idCardNo, String status, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        //老人姓名，模糊查询，这里进行预处理
        name = StrUtil.isBlank(name) ? null : "%" + name + "%";
        Page<ElderPageQueryVo> page = elderMapper.pageQuery(name, idCardNo, status);
        return PageResponse.of(page, ElderPageQueryVo.class);
    }

    /**
     * 老人入住信息查询
     *
     * @param id 老人id
     * @return 老人入住信息
     */
    @Override
    public ElderCheckInInfoVo checkInInfo(Long id) {
        //查询老人入住相关信息
        ElderCheckInInfoVo elderCheckInInfoVo = elderMapper.checkInInfo(id);

        //查询老人关联的护理员
        List<UserVo> userVoList = nursingElderMapper.selectUserByElderId(id);

        //将护理员名称以逗号分隔
        List<String> realNameList = userVoList.stream().map(UserVo::getRealName).distinct().collect(Collectors.toList());
        String realNameStr = StrUtil.join(",", realNameList);
        elderCheckInInfoVo.setNursingName(realNameStr);
        return elderCheckInInfoVo;
    }
}


