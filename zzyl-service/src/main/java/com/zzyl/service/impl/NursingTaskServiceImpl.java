package com.zzyl.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.zzyl.base.PageResponse;
import com.zzyl.entity.NursingElder;
import com.zzyl.entity.NursingTask;
import com.zzyl.entity.User;
import com.zzyl.mapper.NursingElderMapper;
import com.zzyl.mapper.NursingTaskMapper;
import com.zzyl.mapper.UserMapper;
import com.zzyl.service.NursingTaskService;
import com.zzyl.service.OrderService;
import com.zzyl.vo.NursingTaskVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NursingTaskServiceImpl implements NursingTaskService {

    @Autowired
    private NursingTaskMapper nursingTaskMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderService orderService;
    @Autowired
    private NursingElderMapper nursingElderMapper;

    @Override
    public int insert(NursingTask record) {
        return nursingTaskMapper.insert(record);
    }

    @Override
    public NursingTaskVo selectByPrimaryKey(Long id) {
        // 实现方法
        NursingTask nursingTask = nursingTaskMapper.selectByPrimaryKey(id);
        List<NursingElder> nursingElders = nursingElderMapper.selectByElderId(Lists.newArrayList(nursingTask.getElderId()));
        List<Long> collect = nursingElders.stream().map(NursingElder::getNursingId).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(collect)) {
            List<User> users = userMapper.selectUserByIds(collect);
            nursingTask.setNursingName(users.stream().map(User::getRealName).collect(Collectors.toList()));
        }
        return BeanUtil.toBean(nursingTask, NursingTaskVo.class);
    }


    @Override
    public int batchInsert(List<NursingTask> list) {
        return nursingTaskMapper.batchInsert(list);
    }

    /**
     * 取消任务
     *
     * @param taskId 任务ID
     */
    @Override
    public void cancelTask(Long taskId, String reason) {
        // 实现方法
        NursingTask nursingTask = new NursingTask();
        nursingTask.setId(taskId);
        nursingTask.setStatus(3);
        nursingTask.setCancelReason(reason);
        nursingTaskMapper.updateByPrimaryKeySelective(nursingTask);
        NursingTask nursingTask1 = nursingTaskMapper.selectByPrimaryKey(taskId);
    }

    /**
     * 任务改期
     *
     * @param taskId  任务ID
     * @param newTime 新的执行时间
     */
    @Override
    public void rescheduleTask(Long taskId, LocalDateTime newTime) {
        NursingTask nursingTask = new NursingTask();
        nursingTask.setId(taskId);
        nursingTask.setEstimatedServerTime(newTime);
        nursingTaskMapper.updateByPrimaryKeySelective(nursingTask);
    }

    /**
     * 任务执行
     *
     * @param taskId 任务ID
     */
    @Override
    public void executeTask(Long taskId, LocalDateTime realServeTime, String image, String mark) {
        NursingTask nursingTask = new NursingTask();
        nursingTask.setId(taskId);
        nursingTask.setStatus(2);
        nursingTask.setRealServerTime(realServeTime);
        nursingTask.setTaskImage(image);
        nursingTask.setMark(mark);
        nursingTaskMapper.updateByPrimaryKeySelective(nursingTask);
        NursingTask nursingTask1 = nursingTaskMapper.selectByPrimaryKey(taskId);
        if (nursingTask1.getTaskType().equals((byte) 1)) {
            orderService.doOrder(nursingTask1.getRelNo());
        }
    }

    /**
     * 分页查询任务
     *
     * @param page 页码
     * @param size 每页大小
     * @return 任务列表
     */
    @Override
    public PageResponse<NursingTaskVo> getTasksByPage(int page, int size, String elderName, Long nurseId, Long projectId, LocalDateTime startTime, LocalDateTime endTime, Integer status) {
        // 实现方法
        PageHelper.startPage(page, size);
        Page<NursingTask> nursingTasks = nursingTaskMapper.selectByParams(elderName, nurseId, projectId, startTime, endTime, status);
        return PageResponse.of(nursingTasks, NursingTaskVo.class);
    }

}
