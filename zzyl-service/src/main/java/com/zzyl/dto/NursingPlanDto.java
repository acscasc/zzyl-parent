package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import lombok.Data;

import java.util.List;

@Data
public class NursingPlanDto extends BaseDto {

    private Integer sortNo; // 排序号
    private String planName; // 计划名称
    private Integer status; // 状态（0：禁用，1：启用）
    private List<NursingProjectPlanDto> projectPlans;
}
