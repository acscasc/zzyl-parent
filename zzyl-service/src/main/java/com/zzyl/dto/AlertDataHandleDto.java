package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("处理报警数据请求模型")
public class AlertDataHandleDto {

    private String processingResult; //处理结果
    private LocalDateTime processingTime; //处理时间
}