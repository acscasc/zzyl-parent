package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("护理项目计划数据传输对象")
public class NursingProjectPlanDto extends BaseDto {

    private Long planId; // 计划id
    private Long projectId; // 项目id
    private String executeTime; // 计划执行时间
    private Integer executeCycle; // 执行周期  0 天 1 周 2月
    private Integer executeFrequency; // 执行频次
}

