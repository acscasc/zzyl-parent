package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "申请入住请求模型")
public class CheckInApplyDto {

    private CheckInElderDto checkInElderDto; // 老人信息
    private List<ElderFamilyDto> elderFamilyDtoList; // 家属信息
    private CheckInConfigDto checkInConfigDto; // 入住配置
    private CheckInContractDto checkInContractDto; // 签约办理
}
