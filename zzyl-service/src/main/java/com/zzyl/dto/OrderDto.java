package com.zzyl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "订单信息")
public class OrderDto extends BaseDto {

    private Byte paymentStatus; // 支付状态
    private BigDecimal amount; // 订单金额
    private Long projectId; // 项目ID
    private Long elderId; // 老人ID
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime estimatedArrivalTime; // 预计到达时间
}

