package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "申请退住请求模型")
public class RetreatApplyDto {

    private RetreatElderDto retreatElderDto; // 退住老人信息请求模型
    private LocalDateTime releaseDate; // 解除合同时间
    private String releasePdfUrl; // 解除合同url
    private String billJson; // 账单信息json
    private RefundVoucherDto refundVoucherDto; // 退款凭证上传请求模型
}
