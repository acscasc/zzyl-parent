package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "预付费充值记录")
public class PrepaidRechargeRecordDto extends BaseDto {

    private BigDecimal rechargeAmount; // 充值金额
    private String rechargeVoucher; // 充值凭证
    private String rechargeMethod; // 充值方式
    private Long elderId; // 老人ID
    private String elderName; // 老人姓名
    private String bedNo; // 床位号
}
