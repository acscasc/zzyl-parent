package com.zzyl.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("欠款(月度欠费)请求模型")
public class ArrearageDto {

    private String code; // 账单编码
    private int type; // 账单类型 (0:月度账单, 1:订单)
    private String billMonth; // 账单月份
    private BigDecimal amount; // 应付金额
}
