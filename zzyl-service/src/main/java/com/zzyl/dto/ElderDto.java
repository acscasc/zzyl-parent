package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 老人实体类
 */
@Data
@ApiModel(description = "老人实体类")
public class ElderDto extends BaseDto {

    private String name; // 姓名
    private String image; // 头像
    private Integer status; // 状态
    private String idCardNo; // 身份证号
    private String phone; // 手机号
    private String age; // 年龄
    private String sex; // 性别
}

