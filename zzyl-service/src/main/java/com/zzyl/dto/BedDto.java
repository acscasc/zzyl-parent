package com.zzyl.dto;

import com.zzyl.base.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("床位信息")
public class BedDto extends BaseDto {
    @ApiModelProperty("床位编号")
    private String bedNumber; // 床位编号
    @ApiModelProperty("房间ID")
    private Long roomId; // 房间ID
    @ApiModelProperty("排序号")
    private Integer sort; // 排序号
}
