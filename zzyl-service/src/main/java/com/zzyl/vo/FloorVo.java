package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.util.List;

@Data
public class FloorVo extends BaseVo {

    private String name; // 楼层名称
    private Integer code; // 楼层编号
    private List<RoomVo> roomVoList; // 房间列表
}
