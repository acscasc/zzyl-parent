package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel("退款凭证响应模型")
public class RefundVoucherVo {

    private String applicat; // 申请人
    private String tradingChannel; // 退款渠道【支付宝、微信、现金】
    private String refundVoucherUrl; // 退款凭证URL
    private BigDecimal refundAmount; // 退款金额
    private String remark; // 备注
    private LocalDateTime createTime; // 创建时间
}
