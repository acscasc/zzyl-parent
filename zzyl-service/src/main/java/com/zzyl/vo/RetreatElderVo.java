package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(description = "退住老人信息响应模型")
public class RetreatElderVo {

    private Long id; // 老人id
    private String name; // 老人姓名
    private String idCardNo; // 老人身份证号
    private LocalDateTime checkOutTime; // 退住时间，格式：yyyy-MM-dd HH:mm:ss
    private String reason; // 退住原因
    private LocalDateTime checkInStartTime; // 入住开始时间
    private LocalDateTime checkInEndTime; // 入住结束时间
    private LocalDateTime costStartTime; // 费用开始时间
    private LocalDateTime costEndTime; // 费用结算时间
    private String nursingLevelName; // 护理等级名称
    private String bedNumber; // 床位号
    private String nursingName; // 护理员名称，多个护理员以逗号隔开
    private String phone; // 联系方式
    private String address; // 家庭住址
    private String applicat; // 申请人
}
