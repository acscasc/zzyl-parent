package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "NursingLevelVo对象", description = "护理等级VO类")
public class NursingLevelVo extends BaseVo {

    private String name; // 等级名称
    private String planName; // 护理计划名称
    private Long planId; // 护理计划ID
    private BigDecimal fee; // 护理费用
    private Integer status; // 状态（0：禁用，1：启用）
    private String description; // 等级说明
    private Long cid; // 配置ID
}
