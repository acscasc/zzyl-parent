package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("设备服务调用数据响应模型")
public class DeviceServiceDataVo {

    private Long nextTime; // 下一页面中的服务调用记录的起始时间，调用本接口查询下一页服务调用记录时，因为我们做的倒序排列，该值作为请求endTime的值。
    private Boolean nextValid; // 是否有下一页服务调用记录
    private List<ServiceInfo> serviceInfoList; // 服务调用记录集合

    @Data
    @ApiModel("服务调用记录")
    public static class ServiceInfo {
        private String identifier; // 服务标识符
        private String inputData; // 服务的输入参数，MAP格式的字符串，结构为key:value
        private String name; // 服务名称
        private String outputData; // 服务的输出参数，MAP格式的字符串，结构为key:value
        private String time; // 调用服务的时间
    }
}
