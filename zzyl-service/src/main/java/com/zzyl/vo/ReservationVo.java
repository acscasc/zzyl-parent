package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ReservationVo extends BaseVo {

    private String name; // 预约人
    private String mobile; // 预约人手机号
    private LocalDateTime time; // 预约时间
    private String visitor; // 探访人
    private Integer type; // 预约类型，0：参观预约，1：探访预约
    private Integer status; // 预约状态，0：待报道，1：已完成，2：取消，3：过期
}
