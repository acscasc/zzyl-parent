package com.zzyl.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("产品信息响应模型")
public class ProductVo {

    private String productKey; // 产品的ProductKey,物联网平台产品唯一标识
    private String productName; // 产品名称
}
