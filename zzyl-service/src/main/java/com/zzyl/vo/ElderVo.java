package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "老人实体类")
public class ElderVo extends BaseVo {

    private String name; // 姓名
    private String image; // 头像
    private Integer status; // 状态
    private String idCardNo; // 身份证号
    private String phone; // 手机号
    private BigDecimal arrearsAmount; // 欠费金额
    private LocalDateTime paymentDeadline; // 支付截止时间
}


