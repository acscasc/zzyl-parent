package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "护理计划")
public class NursingPlanVo extends BaseVo {

    private Integer sortNo; // 护理计划排序号
    private String planName; // 护理计划名称
    private Integer status; // 状态（0：禁用，1：启用）
    private List<NursingProjectPlanVo> projectPlans; // 护理计划项目计划列表
    private Long lid; // 护理等级id
    private Integer count; // 护理等级绑定数量
}
