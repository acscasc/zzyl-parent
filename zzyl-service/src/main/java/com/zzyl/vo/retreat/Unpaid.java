package com.zzyl.vo.retreat;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(description = "未缴（订单）")
public class Unpaid {

    private Long id; // 账单id
    private String code; // 账单编码
    private int type; // 账单类型 (0:月度账单, 1:订单)
    private BigDecimal amount; // 可退金额
    private String nursingName; // 护理项目
}
