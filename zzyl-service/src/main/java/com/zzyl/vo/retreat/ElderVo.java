package com.zzyl.vo.retreat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 老人实体类
 */
@Data
@ApiModel(description = "老人实体类")
public class ElderVo extends BaseVo {

    private String name; // 姓名
    private String image; // 头像
    private Integer status; // 状态
    private String idCardNo; // 身份证号
    private String phone; // 手机号
    private String sex; // 性别，0：男，1：女，2：未知
    private String birthday; // 出生日期，格式：yyyy-MM-dd
    private String address; // 家庭住址
    private String idCardNationalEmblemImg; // 身份证国徽面
    private String idCardPortraitImg; // 身份证人像面
    private BigDecimal arrearsAmount; // 欠费金额
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime paymentDeadline; // 支付截止时间
}


