package com.zzyl.vo;

import com.zzyl.base.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(description = "房间信息")
public class RoomVo extends BaseVo {

    private Long id; // 房间id
    private String code; // 房间编号
    private Integer sort; // 排序
    private String typeName; // 房间类型名称
    private Long floorId; // 楼层id
    private String floorName; // 楼层名称
    private Double occupancyRate; // 入住率
    private Integer totalBeds; // 总床位数
    private Integer occupiedBeds; // 入住床位数
    private Integer status = 0;
    private BigDecimal price; // 床位费用

    private List<BedVo> bedVoList; // 床位列表
    private List<DeviceVo> deviceVos; // 设备列表
}
