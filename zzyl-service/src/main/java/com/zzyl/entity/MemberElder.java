package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import com.zzyl.vo.BedVo;
import com.zzyl.vo.DeviceVo;
import com.zzyl.vo.RoomVo;
import com.zzyl.vo.retreat.ElderVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 客户老人关联实体类
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MemberElder extends BaseEntity {

    private Long memberId; // 客户id
    private Long elderId; // 老人id
    private ElderVo elderVo;
    private BedVo bedVo;
    private RoomVo roomVo;
    private Map deviceDataVos = new HashMap(16);
    private List<DeviceVo> deviceVos; // 老人关联的设备
}

