package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AlertRule extends BaseEntity {

    private String alertEffectivePeriod;//报警生效时段
    private Integer alertDataType;//报警数据类型，0：老人异常数据，1：设备异常数据
    private String alertRuleName;//告警规则名称
    private Integer alertSilentPeriod;//报警沉默周期
    private String deviceName;//设备名称
    private Integer duration;//持续周期
    private String functionId;//功能标识
    private String functionName;//功能名称
    private String moduleId;//模块的key
    private String moduleName;//模块名称
    private String operator;//运算符
    private String productKey;//所属产品的key
    private String productName;//产品名称
    private String iotId;//物联网设备id
    private String remark;//备注
    private Integer status;//0 禁用 1启用
    private Float value;//阈值
}