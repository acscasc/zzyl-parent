package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "预约信息")
public class Reservation extends BaseEntity {

    private String name; // 预约人
    private String mobile; // 预约人手机号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time; // 时间
    private String visitor; // 探访人
    private Integer type; // 预约类型，0：参观预约，1：探访预约
    private Integer status; // 预约状态，0：待报道，1：已完成，2：取消，3：过期
}
