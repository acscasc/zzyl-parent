package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DeviceData extends BaseEntity {

    private String accessLocation;//接入位置
    private Integer locationType;//位置类型 0：随身设备 1：固定设备
    private Integer physicalLocationType;//物理位置类型 0楼层 1房间 2床位
    private String deviceDescription;//位置备注
    private LocalDateTime alarmTime;//报警时间
    private String dataValue;//数据值
    private String deviceName;//设备名称
    private String functionId;//功能标识符
    private String iotId;//物联网设备ID
    private String nickname;//备注名称
    private String productKey;//所属产品的key
    private String productName;//产品名称
}