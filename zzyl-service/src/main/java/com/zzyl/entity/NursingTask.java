package com.zzyl.entity;

import com.zzyl.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NursingTask extends BaseEntity {

    private Long nursingId; // 护理员ID
    private Long projectId; // 项目id
    private Long elderId; // 老人ID
    private String bedNumber; // 床位编号
    private Byte taskType; // 任务类型(2:月度任务  1:订单任务)
    private LocalDateTime estimatedServerTime; // 预计服务时间
    private LocalDateTime realServerTime; // 实际服务时间
    private String mark; // 执行记录
    private String cancelReason; // 取消原因
    private Integer status; // 状态  1待执行 2已执行 3已关闭
    private String relNo; // 关联单据编号
    private String taskImage; // 执行图片
    private String projectName; // 护理项目名称
    private String elderName; // 老人姓名
    private String idCardNo; // 身份证号
    private String image; // 头像
    private String sex;
    private List<String> nursingName;
    private String lName;
}