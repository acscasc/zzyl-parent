package com.zzyl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zzyl.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退住实体类")
public class Retreat extends BaseEntity {

    private Long id; // 主键
    private String retreatCode; // 退住单号
    private String title; // 退住标题
    private Long elderId; // 老人id
    private String name; // 老人姓名
    private String idCardNo; // 身份证号
    private String phone; // 联系方式
    private String address; // 家庭住址
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInStartTime; // 入住开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkInEndTime; // 入住结束时间
    private LocalDateTime costStartTime; // 费用开始时间
    private LocalDateTime costEndTime; // 费用结算时间
    private String nursingLevelName; // 护理等级
    private String bedNumber; // 入住床位
    private String nursingName; // 护理员名称
    private String contractName; // 签约合同
    private String contractUrl; // 合同URL
    private String contractNo; // 合同编号
    private LocalDateTime checkOutTime; // 退住时间
    private String reason; // 退住原因
    private String remark; // 备注
    private String applicat; // 申请人
    private String deptNo; // 申请人部门编号
    private Long applicatId; // 申请人id
    private LocalDateTime createTime; // 申请时间
    private String taskId; // 任务ID
}
