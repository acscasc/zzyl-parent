package com.zzyl.mapper;

import com.zzyl.entity.RetreatBill;
import org.apache.ibatis.annotations.*;

@Mapper
public interface RetreatBillMapper {

    /**
     * 新增退住账单
     *
     * @param retreatBill 退住账单
     */
    void insert(RetreatBill retreatBill);

    /**
     * 根据退住id查询退住账单
     *
     * @param retreatId 退住id
     * @return 退住账单信息
     */
    @Select("select * from retreat_bill where retreat_id = #{retreatId}")
    RetreatBill selectByRetreatId(@Param("retreatId") long retreatId);

    @Update("update retreat_bill set  bill_json = #{billJson} where retreat_id = #{retreatId}")
    void updateBillJsonByRetreatId(@Param("billJson") String billJson,
                                   @Param("retreatId") long retreatId);

    @Delete("delete from retreat_bill where retreat_id = #{retreatId} ")
    void deleteByByRetreatId(long id);

    void update(RetreatBill retreatBill);

}