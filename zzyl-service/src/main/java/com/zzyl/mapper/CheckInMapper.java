package com.zzyl.mapper;

import com.github.pagehelper.Page;
import com.zzyl.entity.CheckIn;
import com.zzyl.vo.CheckInPageQueryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface CheckInMapper {

    int deleteByPrimaryKey(Long id);

    /**
     * 新增入住记录
     *
     * @param checkIn 入住信息
     * @return 新增条数
     */
    int insert(CheckIn checkIn);

    int updateByPrimaryKeySelective(CheckIn checkIn);

    int updateByPrimaryKey(CheckIn record);

    /**
     * 分页查询
     *
     * @param elderName 老人姓名，模糊查询
     * @param idCardNo  身份证号，精确查询
     * @param status    入住状态{@link com.zzyl.enums.CheckInStatusEnum}
     * @return 分页结果
     */
    Page<CheckInPageQueryVo> selectByPage(@Param("elderName") String elderName,
                                          @Param("idCardNo") String idCardNo,
                                          @Param("status") Integer status);

    /**
     * 根据id查询
     *
     * @param id 入住id
     * @return 入住信息
     */
    @Select("select * from check_in where id=#{id}")
    CheckIn selectById(@Param("id") Long id);

    /**
     * 根据老人id更新入住状态
     *
     * @param elderId 老人id
     * @param status  入住状态{@link com.zzyl.enums.CheckInStatusEnum}
     */
    @Update("update check_in set status=#{status} where elder_id=#{elderId}")
    void updateStatusByElderId(@Param("elderId") Long elderId,
                               @Param("status") Integer status);

    /**
     * 根据老人id和状态查询
     *
     * @param elderId 老人id
     * @param status  入住状态{@link com.zzyl.enums.CheckInStatusEnum}
     * @return 入住信息
     */
    @Select("select * from check_in where elder_id=#{elderId} and status=#{status}")
    CheckIn selectByElderIdAndStatus(@Param("elderId") Long elderId,
                                     @Param("status") int status);
}