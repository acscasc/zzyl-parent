package com.zzyl.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 入住状态
 *
 * @author itcast
 */
@Getter
@AllArgsConstructor
public enum CheckInStatusEnum {

    PROGRESSING(0, "入住中"),

    RETREAT(1, "已退住");
    /**
     * 序号值
     */
    private final int ordinal;

    /**
     * 描述
     */
    private final String name;
}
