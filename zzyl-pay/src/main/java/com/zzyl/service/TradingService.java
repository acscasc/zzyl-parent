package com.zzyl.service;

import com.zzyl.entity.Trading;
import com.zzyl.enums.TradingStateEnum;
import com.zzyl.vo.TradingVo;

import java.util.List;

/**
 * 交易订单表 服务类
 */
public interface TradingService {

    /**
     * 创建交易单
     *
     * @param trading 交易单信息
     * @return 交易单
     */
    TradingVo createTrading(Trading trading);

    /***
     * 按交易单号查询交易单
     *
     * @param tradingOrderNo 交易单号
     * @return 交易单数据
     */
    Trading findTradByTradingOrderNo(Long tradingOrderNo);

    /***
     * 按订单单号查询交易单
     * @param productOrderNo 交易单号
     * @param tradingType
     * @return 交易单数据
     */
    Trading findTradByProductOrderNo(Long productOrderNo, String tradingType);

    /***
     * 按交易状态查询交易单，按照时间正序排序
     * @param tradingState 状态
     * @param count 查询数量，默认查询10条
     * @return 交易单数据列表
     */
    List<Trading> findListByTradingState(TradingStateEnum tradingState, Integer count);

    Boolean saveOrUpdate(Trading trading);

    /**
     * 关闭交易单
     *
     * @param tradingVo 交易单
     * @return 已关闭的交易单
     */
    TradingVo closeTrading(TradingVo tradingVo);

    /**
     * 交易单退款
     *
     * @param tradingVo 交易单
     * @return 已退款交易单
     */
    TradingVo refundTrading(TradingVo tradingVo);

    /***
     *  统一收单线下交易查询
     * 该接口提供所有支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。
     * @param tradingVo 交易单
     * @return 交易单
     */
    TradingVo queryTrading(TradingVo tradingVo);
}
